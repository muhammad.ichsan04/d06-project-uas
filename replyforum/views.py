from django.http import response
from django.contrib.auth import get_user
from django.shortcuts import render, HttpResponseRedirect
from .forms import ReplyForumForm
from django.contrib.auth.decorators import login_required
from .models import ReplyForum
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.

def json(request):
    data = serializers.serialize('json', ReplyForum.objects.all())
    return HttpResponse(data, content_type="application/json") 

@login_required()
def replyforum(request):
    context = {}
    user = get_user(request)
    form = ReplyForumForm(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        obj = form.save(commit=False)
        obj.user = request.user
        obj.save()
        return HttpResponseRedirect("/forum")
    else:
        context['form'] = form
        return render(request, "replyforum_form.html", context)

@login_required()
def createpost(request):
    context = {}
    form = ReplyForumForm(request.POST, user=request.user)
    if form.is_valid() and request.method == 'POST':
        profile = form.save(commit=False)
        profile.user = request.user
        profile.save()
        return HttpResponseRedirect("/forum")
    else:
        form = ReplyForumForm(user = request.user)
        return render(request, "createpost_form.html", {'form' : form})

@login_required()
def editpost(request):
    context = {}
    form = ReplyForumForm(request.POST, user=request.user)
    if form.is_valid() and request.method == 'POST':
        profile = form.save(commit=False)
        profile.user = request.user
        profile.save()
        return HttpResponseRedirect("/forum")
    else:
        form = ReplyForumForm(user = request.user)
        return render(request, "editpost_form.html", context)


'''
    repforum = ReplyForum.objects.get(id=pk)
    form = ReplyForumForm(instance=repforum)

    if request.method == 'POST':
        form = ReplyForumForm(request.POST, instance=repforum)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/forum")

    context = {'form':form}
    user = get_user(request)
    return render(request, "editpost_form.html", context)
'''

@login_required()
def deletepost(request):
    context = {}
    return render(request, "delete_post.html", context)

@login_required()
def pinpost(request):
    context = {}
    return render(request, "pin_post.html", context)
