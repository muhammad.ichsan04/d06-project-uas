# Generated by Django 3.2.7 on 2021-12-28 17:08

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('replyforum', '0007_rename_author_replyforum_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='replyforum',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
