from django.urls import path
from .views import home, random, detail, search_recipe, detail_recipe, random_recipe

app_name='recipe'

urlpatterns = [
    path('', home, name='home'),
    path('<int:id>/', detail, name='detail'),
    path('random/', random, name='random'),
    path('api/search/<keyword>/', search_recipe, name='search_recipe'),
    path('api/detail/<int:id>/', detail_recipe, name='detail_recipe'),
    path('api/random/', random_recipe, name='random_recipe'),
]
