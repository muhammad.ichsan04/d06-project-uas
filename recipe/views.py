from django.contrib.auth.decorators import login_required
from django.http import response
from django.shortcuts import render
from commentrecipe.models import Comment
import requests, os

def home(request):
    background_response = requests.get("https://api.unsplash.com/photos/random?topics=xjPR4hlkBGA&orientation=landscape&client_id=uv_ChNikCbCCOyRPcVW54BHoKGfTUI_HdKJXZ4C2uQ0")
    background_image = background_response.json()["urls"]["regular"]
    response = {'bg_image' : background_image, 'RECIPE_API' : os.getenv('RECIPE_API')}
    return render(request, 'recipe_home.html', response)

@login_required()
def random(request):
    response_data = requests.get("https://api.spoonacular.com/recipes/random?apiKey=f00a4b4fb964493fb2cd468db364a7d3")
    try:
        response = response_data.json()["recipes"][0]
        response['id_number'] = response_data.json()["recipes"][0]['id']
    except:
        return render(request, 'recipe_home.html', {'bg_image' : "https://i.ibb.co/888MFvv/Sorry-The-Recipe-API-requests-has-reached-the-limit-Please-try-again-tomorrow-1.jpg", "limit": True})
    return render(request, 'recipe_detail.html', response)

@login_required()
def detail(request, id):
    response_data = requests.get("https://api.spoonacular.com/recipes/" + str(id) + "/information?apiKey=f00a4b4fb964493fb2cd468db364a7d3")
    response = response_data.json()
    response["id_number"] = id
    response["comments"] = Comment.objects.filter(id_number=id)
    return render(request, 'recipe_detail.html', response)

def search_recipe(request, keyword):
    response_data = requests.get('https://api.spoonacular.com/recipes/complexSearch?query='+keyword+'&apiKey=093c3f2d670c4f29937ce2bf0b69b565')
    return response.JsonResponse(response_data.json())

def detail_recipe(request, id):
    response_data = requests.get("https://api.spoonacular.com/recipes/" + str(id) + "/information?apiKey=f00a4b4fb964493fb2cd468db364a7d3")
    return response.JsonResponse(response_data.json())

def random_recipe(request):
    response_data = requests.get("https://api.spoonacular.com/recipes/random?apiKey=f00a4b4fb964493fb2cd468db364a7d3")
    return response.JsonResponse(response_data.json())
