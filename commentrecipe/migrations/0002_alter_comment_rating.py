# Generated by Django 3.2.7 on 2021-10-28 08:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('commentrecipe', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='rating',
            field=models.CharField(max_length=1),
        ),
    ]
