from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from .models import Comment
from .forms import CommentForm
from django.http.response import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.http import response, HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions


# Create your views here.
@csrf_exempt
def json(request):
    data = serializers.serialize('json', Comment.objects.all())
    return HttpResponse(data, content_type="application/json")


def comment(request, id):
    form = CommentForm(request.POST or None)
    
    if form.is_valid():
        # save the form data to model
        instance = form.save(commit=False)
        instance.id_number = id
        instance.save()
        return HttpResponseRedirect("/recipe/" + str(id))

    response = {'form': form}
    return render(request, 'comment.html', response)


#class CommentSerializer(serializers.ModelSerializer) :
#    class Meta:
 #       model = Comment
 #       fields = ['tanggal', 'judul', 'isi', 'user']

