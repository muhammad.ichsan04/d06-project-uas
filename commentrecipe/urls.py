from django.urls import path
from .views import comment, json

urlpatterns = [
    path('<int:id>/', comment, name='comment'),
    path('json', json, name='json'),
]