from commentrecipe.views import comment
from rest_framework import serializers
from django.contrib.auth.models import User
from commentrecipe.models import Comment

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password']
        # fields = '__all__'

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
        )
        
        return user

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['id_number', 'name', 'rating', 'comment']
        # fields = '__all__'

    def create(self, validated_data):
        comment = Comment.objects.create(
            id_number=validated_data['id_number'],
            name=validated_data['name'],
            rating=validated_data['rating'],
            comment=validated_data['comment']
        )
        
        return comment