from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import render

from django.http import JsonResponse
from rest_framework.decorators import api_view, permission_classes
from django.views.decorators.csrf import csrf_exempt
from commentrecipe.views import comment
from rest_framework.response import Response
from .serializer import CommentSerializer, UserSerializer
from api import serializer
from rest_framework.permissions import AllowAny
from commentrecipe.models import Comment 

# Create your views here.
@api_view(['GET'])
@permission_classes([AllowAny])
@csrf_exempt
def apiOverview(request):
    api_urls = {
        'List':'/user-list/',
        'Detail View':'/user-detail/<str:pk>/',
        'Detail View Username':'/user-detail-username/<str:pk>/',
        'Create':'/user-create/',
        'Update':'/user-update/<str:pk>/',
        'Delete':'/user-delete/<str:pk>/',
        'Login':'/user-login/<str:pk>/',
    }

    return Response(api_urls)

# Function to get all users and their passwords as a json
@api_view(['GET'])
@permission_classes([AllowAny])
@csrf_exempt
def userList(request):
    users = User.objects.all()
    serializer = UserSerializer(users, many=True)

    return Response(serializer.data)

# Function to get a specific user by id and its password as json
@api_view(['GET'])
@permission_classes([AllowAny])
@csrf_exempt
def userDetail(request, pk):
    user = User.objects.get(id=pk)
    serializer = UserSerializer(user, many=False)

    return Response(serializer.data)

# Function to get a specific user by name and its password as json
# @api_view(['GET'])
# def userDetailUsername(request, pk):
#     user = User.objects.get(username=pk)
#     serializer = UserSerializer(user, many=False)

#     return Response(serializer.data)

# Function to create a specific user and its password
@api_view(['POST', 'GET'])
@permission_classes([AllowAny])
@csrf_exempt
def userCreate(request):
    serializer = UserSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

# Function to update a specific user by id
@api_view(['POST'])
@permission_classes([AllowAny])
@csrf_exempt
def userUpdate(request, pk):
    user = User.objects.get(id=pk)
    serializer = UserSerializer(instance=user, data=request.data)

    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

# Function to remove a specific user by id
@api_view(['DELETE'])
@permission_classes([AllowAny])
@csrf_exempt
def userDelete(request, pk):
    user = User.objects.get(id=pk)
    user.delete()

    return Response('User successfully deleted')   

# Function to login a specific user by input of their username and password
@api_view(['POST', 'GET'])
@permission_classes([AllowAny])
@csrf_exempt
def userLogin(request):

    username = request.data.get('username') 
    password = request.data.get('password')

    user = authenticate(request, username=username, password=password)

    if user is not None:
        login(request, user)

        response = {
            'response':'valid',
            'userId':request.user.id,
            'username':request.user.username,
            }

        logout(request)

        return Response(response)

    else:
        return Response({
            'response':'invalid',
            'userId':'null',
            'username':'null',
        })

@api_view(['POST', 'GET'])
@permission_classes([AllowAny])
def commentCreate(request):
    serializer = CommentSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

@api_view(['GET'])
@permission_classes([AllowAny])
def viewList(request):
    comments = Comment.objects.all()
    serializer = CommentSerializer(comments, many=True)

    return Response(serializer.data)