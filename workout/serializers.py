from rest_framework import serializers
from .models import Video

class VideoSerializer(serializers.Serializer):
    vidTitle = serializers.CharField(max_length=120)
    vidAdded = serializers.DateTimeField()
    vidUrl = serializers.CharField(max_length=120)
    
    def create(self, validated_data):
        return Video.objects.create(**validated_data)