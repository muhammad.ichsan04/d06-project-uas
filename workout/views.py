from django.shortcuts import render, redirect
from .forms import VideoForm
from .models import Video
from django.contrib.auth.decorators import login_required
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import VideoSerializer

# Create your views here.
def index(request):
  vids = Video.objects.all()
  return render(request, 'workout.html', {"vids":vids})

@login_required(login_url = '/admin/login/')
def add_video(request):
    context = {}
    form = VideoForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        response = redirect("/workout")
        return response

    context['form'] = form
    return render(request, "add.html", context)
  
class VideoView(APIView):
    def get(self, request):
        videos = Video.objects.all()
        serializer = VideoSerializer(videos, many=True)
        return Response({"videos": serializer.data})
    
    def post(self, request):
        videos = request.data.get('videos')[0]

        # Create a video from the above data
        serializer = VideoSerializer(data=videos)
        if serializer.is_valid(raise_exception=True):
            video_saved = serializer.save()
        
        return Response({"success": "Video '{}' created successfully".format(video_saved.vidTitle)})