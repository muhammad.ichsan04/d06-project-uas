from django.db import models
# Create your models here.

class Video(models.Model):
    vidTitle = models.CharField(max_length=120)
    vidAdded = models.DateTimeField(auto_now_add=True)
    vidUrl = models.CharField(max_length=120)
    def __str__(self):
        return self.vidTitle