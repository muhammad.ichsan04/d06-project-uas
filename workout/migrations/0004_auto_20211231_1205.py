# Generated by Django 3.2.7 on 2021-12-31 05:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workout', '0003_auto_20211028_1352'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='vidTitle',
            field=models.CharField(max_length=120),
        ),
        migrations.AlterField(
            model_name='video',
            name='vidUrl',
            field=models.CharField(max_length=120),
        ),
    ]
