from django.urls import path
from .views import index, add_video, VideoView

urlpatterns = [
    path('', index, name='index'),
    path('add', add_video),
    path('videos/', VideoView.as_view()),
]