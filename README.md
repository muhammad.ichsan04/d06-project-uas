[![pipeline status](https://gitlab.com/muhammad.ichsan04/d06-project/badges/master/pipeline.svg)](https://gitlab.com/muhammad.ichsan04/d06-project/-/commits/master)
[![coverage report](https://gitlab.com/muhammad.ichsan04/d06-project/badges/master/coverage.svg)](https://gitlab.com/muhammad.ichsan04/d06-project/-/commits/master)  
# d06-project
Kelompok D06
> [Link Heroku](https://d06-project.herokuapp.com/)

## Anggota:  
- 2006596535	Fransisco William Sudianto  
- 2006596333	Debra Mazaya  
- 2006596264	Muhammad Ichsan Khairullah  
- 2006596655	Shadqi Marjan Sadiya  
- 2006596024	Elfraera Winson Barry L. Tobing  
- 2006595740	Muhammad Zaki Ash-shidiqi  
- 2006595860	Adietya Christian  

## Penjelasan:  
- Website ini menyediakan berbagai informasi yang dibutuhkan selama masa pandemi seperti video workout yang bisa dilakukan 
di rumah, resep makanan, update kasus covid, serta forum untuk sharing  

## Modul:  
1. Menyediakan reply untuk resep makanan  
2. Menyediakan video workout  
3. Menyediakan resep makanan  
4. Menyediakan update kasus covid  
5. Menyediakan forum sharing  
6. Menyediakan sarana login user  
7. Menyediakan reply untuk forum  

## Persona:  
- Admin : Dapat melakukan maintenance dan development website  
- User  : Dapat mengakses fitur-fitur website seperti melihat resep makanan, video workout, 
forum sharing, dan update status covid  
- Guest : Dapat melihat informasi yang ada di website tetapi tidak bisa post atau reply dalam forum  

test