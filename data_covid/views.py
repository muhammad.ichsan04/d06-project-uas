from django.shortcuts import render
from django.http import response, JsonResponse
import requests
import json
# Create your views here.

def data_indo():
    api_url = "https://api.kawalcorona.com/indonesia/"
    result = requests.get(api_url).json()[0]
    data = {
        'name': result['name'],
        'positive': result['positif'],
        'recovered': result['sembuh'],
        'deaths': result['meninggal']
    }
    return data

def data_global():
    api_url_sembuh = "https://api.kawalcorona.com/sembuh/"
    api_url_meninggal = "https://api.kawalcorona.com/meninggal/"
    api_url_positif = "https://api.kawalcorona.com/positif/"
    result_sembuh = requests.get(api_url_sembuh).json()
    result_meninggal = requests.get(api_url_meninggal).json()
    result_positif = requests.get(api_url_positif).json()

    data = {
        'name': 'Global',
        'positive': result_positif['value'],
        'recovered': result_sembuh['value'],
        'deaths': result_meninggal['value']
    }
    return data

def data_provinsi():
    # api_url = "https://indonesia-covid-19.mathdro.id/api/provinsi/"
    api_url = "https://data.covid19.go.id/public/api/prov.json"
    result = requests.get(api_url).json()
    return result

def data_covid(request):
    return render(request, "data_covid/data_covid.html")

def ajax_data(request):
    json_indo = data_indo()
    json_global = data_global()
    json_provinsi = data_provinsi()
    data = {
        "indonesia" : json_indo,
        "global" : json_global,
        "provinsi" : json_provinsi,
    }
    return JsonResponse(data)
