$.ajax({
  url: '/covid-19/ajax/get_data',
}).done(data => {
  let html_provinsi = "";
  let html_indo_global = "";
  var arrayData = Object.values(data);
  // console.log(arrayData)
  for (let i = 0 ; i < arrayData.length; i++){
    if (i == 2){
      for (let j = 0; j < arrayData[i].list_data.length-1; j++){
        html_provinsi += `
        <div class="col d-flex justify-content-center">
          <div class="card h-100 text-center" style="width: 300px;">
            <div class="card-body">
              <h4 class="card-title">${arrayData[i].list_data[j].key}</h3>
              <h6 class="card-text">Positif</h5>
              <h6 class="card-text">${arrayData[i].list_data[j].jumlah_kasus}</h5>
              <h6 class="card-text">Sembuh</h5>
              <h6 class="card-text">${arrayData[i].list_data[j].jumlah_sembuh}</h5>
              <h6 class="card-text">Meninggal</h5>
              <h6 class="card-text">${arrayData[i].list_data[j].jumlah_meninggal}</h5>
            </div>
          </div>
        </div>
        `;
      }
    } else {
      html_indo_global += `
      <div class="col d-flex justify-content-center">
        <div class="card h-100 text-center" style="width: 370px;">
          <div class="card-body">
            <h4 class="card-title">${arrayData[i].name}</h3>
            <h6 class="card-text">Positif</h5>
            <h6 class="card-text">${arrayData[i].positive}</h5>
            <h6 class="card-text">Sembuh</h5>
            <h6 class="card-text">${arrayData[i].recovered}</h5>
            <h6 class="card-text">Meninggal</h5>
            <h6 class="card-text">${arrayData[i].deaths}</h5>
          </div>
        </div>
      </div>
      `;
    }
  }
  document.getElementById("ajax-indo-global").innerHTML = html_indo_global;
  document.getElementById("ajax-provinsi").innerHTML = html_provinsi;
});
  