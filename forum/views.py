from django.http import response, HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from django.shortcuts import render
from replyforum.models import ReplyForum
from .serializers import ForumSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from rest_framework import permissions

@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def getRoutes(request):
    routes = [
        {
            'Endpoint' : '/replies',
            'method' : 'GET',
            'body' : None,
            'description' : 'Returns an array of replies'
        },
        {
            'Endpoint': '/notes/id',
            'method': 'GET',
            'body': None,
            'description': 'Returns a single reply object'
        },
        {
            'Endpoint': 'Notes/id/update/',
            'method' : 'PUT',
            'body': {'body': ""},
            'description': 'Creates an existing reply with data sent'
        },
        {
            'Endpoint': '/notes/id/delete/',
            'method': 'DELETE',
            'body': None,
            'description': 'Deletes an existing note'
        },
    ]
    return Response(routes)

@api_view(('GET',))
@permission_classes((permissions.AllowAny,))
def getReplies(request):
    replies = ReplyForum.objects.all()
    serializer = ForumSerializer(replies, many=True)
    return Response(serializer.data)

@api_view(('GET',))
@permission_classes((permissions.AllowAny,))
def getReply(request, pk):
    reply = ReplyForum.objects.get(id=pk)
    serializer = ForumSerializer(reply, many=False)
    return Response(serializer.data)

# Create your views here.
def forum(request):
    posts = ReplyForum.objects.all().order_by('-tanggal')

    response = {'posts' : posts}
    return render(request, "forum/forum.html", response)