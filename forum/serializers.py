from rest_framework import serializers
from replyforum.models import ReplyForum

class ForumSerializer(serializers.ModelSerializer) :
    class Meta:
        model = ReplyForum
        fields = ['tanggal', 'judul', 'isi', 'user']