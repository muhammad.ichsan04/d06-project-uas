from django.urls import path
from .views import forum, getRoutes, getReplies, getReply
from replyforum.views import json, createpost, replyforum, editpost, deletepost, pinpost

app_name='forum'

urlpatterns = [
    path('', forum, name='forum'),
    path('create-new-post', createpost, name='createpost'),
    path('reply-a-post', replyforum, name='replyforum'),
    path('edit-a-post', editpost, name='editpost'),
    path('delete-a-post', deletepost, name='deletepost'),
    path('pin-a-post', pinpost, name='pinpost'),
    path('json', json, name='json'),
    path('api', getRoutes),
    path('replies/', getReplies),
    path('/replies/<str:pk>/', getReply)
]
