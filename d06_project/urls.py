"""d06_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('api-auth/', include('rest_framework.urls')),
    path('admin/', admin.site.urls),
    path('workout/', include(('workout.urls', 'workout'), namespace='workout')),
    path('', include(('main.urls','main'), namespace='main')),
    path('forum/', include(('forum.urls', 'forum'), namespace='forum')),
    path('accounts/', include(('accounts.urls', 'accounts'), namespace='accounts')),
    path('recipe/', include(('recipe.urls', 'recipe'), namespace='recipe')),
    path('covid-19/', include('data_covid.urls', namespace='data_covid')),
    path('comment/' , include(('commentrecipe.urls', 'commentrecipe'), namespace='commentrecipe')),
    path('replyforum/' , include(('replyforum.urls', 'replyforum'), namespace='replyforum')),
    path('api/', include(('api.urls', 'api'), namespace='api')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

